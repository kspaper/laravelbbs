<?php

namespace App\Transformers;

use App\Models\Topic;
use League\Fractal\TransformerAbstract;

class TopicTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user', 'category', 'topReplies'];
    // 可以嵌套的额外资源有 user 和 category
    // availableIncludes 中的每一个参数都对应一个具体的方法，方法命名规则为 include + user 、 include + category 驼峰命名

    public function transform(Topic $topic)
    {
        return [
            'id' => $topic->id,
            'title' => $topic->title,
            'body' => $topic->body,
            'user_id' => $topic->user_id,
            'category_id' => $topic->category_id,
            'reply_count' => $topic->reply_count,
            'view_count' => $topic->view_count,
            'last_reply_user_id' => $topic->last_reply_user_id,
            'excerpt' => $topic->excerpt,
            'slug' => $topic->slug,
            'created_at' => $topic->created_at->toDateTimeString(),
            'updated_at' => $topic->updated_at->toDateTimeString(),
        ];
    }
    //获取和转换
    public function includeUser(Topic $topic)
    {
        return $this->item($topic->user, new UserTransformer());
        //此方法将查询到的用户数据，$topic->user ，通过 UserTransformer 格式化用户数据
    }

    public function includeCategory(Topic $topic)
    {
        return $this->item($topic->category, new CategoryTransformer());
    }

    public function includeTopReplies(Topic $topic)
    {
        return $this->collection($topic->topReplies, new ReplyTransformer());
    }
}