<?php

namespace App\Http\Requests\Api;

//use Illuminate\Foundation\Http\FormRequest;
use Dingo\Api\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name' => 'required|between:3,25|regex:/^[A-Za-z0-9\-\_]+$/|unique:users,name',
                    'password' => 'required|string|min:6',
                    'verification_key' => 'required|string',
                    'verification_code' => 'required|string',
                ];
                break;
            case 'PUT':
            case 'PATCH':
                $userId = \Auth::guard('api')->id();
                return [
                    'name' => 'between:3,25|regex:/^[A-Za-z0-9\-\_]+$/|unique:users,name,' .$userId,
                    'email' => 'email',
                    'introduction' => 'max:80',
                    'avatar_image_id' => 'exists:images,id,type,avatar,user_id,'.$userId,
                    //images 表中是否存在 id, type 是否为 avatar, 用户id 是否为当前登录 user id
                ];
                break;
        }
    }

    public function attributes()
    {
        return [
            'verification_key' => 'Verification key',
            'verification_code' => 'Verification Code',
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'The username has been occupied',
            'name.regex' => 'The username must be alphbet letter, number, dash(-) or underscore(_)',
            'name.between' => 'The length of username must be 3 - 25',
            'name.required' => 'The username cannot be null',
        ];
    }
}